export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'risis-rcf-rimow-rm',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'intro.js/introjs.css',
    '@/assets/scss/rimow.scss',
    '@/assets/scss/rimow-vendors.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/content-loader.js',
    '~/plugins/filters.js',
    '~/plugins/vue-radial-progress.js',
    '~/plugins/dropzone.js',
    '~/plugins/vuelidate.js',
    '~/plugins/vue-final-modal.js',
    '~/plugins/debounce.js',
    // '~/plugins/axios',
    '~/plugins/tabler-icons',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/composition-api/module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    'nuxt-vue-select',
    '@nuxtjs/auth-next',
    'nuxt-socket-io',
    'intro.js',
  ],

  io: {
    sockets: [
      {
        url: process.env.RIBA_URL,
      },
    ],
  },

  proxy: {
    '/api': process.env.RIBA_PROXY_URL,
  },

  bootstrapVue: {
    icons: false,
    css: false, // Or `css: false`
    bvCSS: false, // Or `bvCSS: false`
    components: [
      'BAlert',
      'BBadge',
      'BButton',
      'BCard',
      'BCardText',
      'BCardHeader',
      'BCardBody',
      'BDropdown',
      'BDropdownItem',
      'BInputGroup',
      'BInputGroupText',
      'BImg',
      'BLink',
      'BSpinner',
    ],
    componentPlugins: [
      'LayoutPlugin',
      'FormPlugin',
      'FormGroupPlugin',
      'FormInputPlugin',
      'FormRadioPlugin',
      'FormCheckboxPlugin',
      'FormTagsPlugin',
      'ModalPlugin',
      'NavbarPlugin',
      'ProgressPlugin',
      'TooltipPlugin',
      'ToastPlugin',
    ],
  },

  axios: {
    proxy: true,
  },

  auth: {
    // plugins: ['~/plugins/axios.js'],
    strategies: {
      keycloak: {
        scheme: 'oauth2',
        endpoints: {
          authorization: `https://${process.env.AUTH_HOSTNAME}/auth/realms/rcf/protocol/openid-connect/auth`,
          token: `https://${process.env.AUTH_HOSTNAME}/auth/realms/rcf/protocol/openid-connect/token`,
          userInfo: `https://${process.env.AUTH_HOSTNAME}/auth/realms/rcf/protocol/openid-connect/userinfo`,
          logout:
            `https://${process.env.AUTH_HOSTNAME}/auth/realms/rcf/protocol/openid-connect/logout?redirect_uri=` +
            encodeURIComponent(String(process.env.BASE_URL)),
        },
        token: {
          property: 'access_token',
          type: 'Bearer',
          maxAge: 60 * 60 * 24,
        },
        refreshToken: {
          property: 'refresh_token',
        },
        responseType: 'code',
        grantType: 'authorization_code',
        accessType: undefined,
        redirectUri: undefined,
        logoutRedirectUri: undefined,
        clientId: 'RIMOW',
        scope: ['openid', 'profile', 'email'],
        state: 'UNIQUE_AND_NON_GUESSABLE',
        codeChallengeMethod: 'S256',
        responseMode: '',
        acrValues: '',
      },
    },
    redirect: {
      login: '/login',
      logout: '/login',
      callback: '/callback',
      home: '/',
    },
  },

  publicRuntimeConfig: {
    apiRoot: '/api',
    datastoreUrl: process.env.DATASTORE_URL,
    baseUrl: process.env.BASE_URL,
    axios: {
      browserBaseURL: process.env.BASE_URL,
    },
    ribaUrl: process.env.RIBA_URL,
    storagePublicUri: process.env.STORAGE_PUBLIC_URI,
  },

  privateRuntimeConfig: {
    ribaToken: process.env.RIBA_TOKEN || 'ABC',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    // vue-final-modal: @see https://v2.vue-final-modal.org/setup#register-plugin-in-nuxt
    transpile: ['vue-radial-progress', 'vue-content-loader', 'vue-final-modal'],
  },
  server: {
    host: '0', // default: localhost
  },
};
