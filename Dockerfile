# Check https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md for reference

FROM node:16-alpine

RUN mkdir -p /home/node/src/.nuxt && chown -R node:node /home/node/src

WORKDIR /home/node/src

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

ARG PORT=3000
ENV PORT=${PORT}

COPY --chown=node:node package*.json yarn.lock ./

USER node

RUN yarn install && yarn cache clean

COPY . .

CMD ["yarn", "dev"]
