# risis-rcf-rimow

## Install dependencies

Both `package.json` and `yarn.lock` have to be updated when adding/removing dependencies to ensure a correct build.

The usual way would be running `yarn add <dependency_name>` in the local directory.  This will install the dependency in the usual `node_modules` folder.

As we run the application in a docker container, we have a fixed node version where the project is built: `node:15-alpine` so if you're running node v15 it should be fine.

Otherwise, to ensure compatibility, you can install dependencies in a temporary container and this way only the `package.json` and `yarn.lock` will be updated.

```shell
docker run --rm -v $(pwd)/package.json:/home/node/app/package.json -v $(pwd)/yarn.lock:/home/node/app/yarn.lock -w /home/node/app/ node:15-alpine yarn add <new_dependency_name>
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Releasing a new RIMOW version

* Go to main branch, merge dev branch into main
* Make sure the new version is declared in package.json file
  * See the [Semantic Versioning](https://semver.org) specification on how to increment version numbers
* Make sure all automated tests are ok
* Push main branch to Gitlab
* Create a git tag for the new version and push to Gitlab
* Merge main branch back to dev branch and push
* Create a new Release on Gitlab from the new tag
  * https://gitlab.com/risis/rcf/risis-rcf-rimow/-/releases

Gitlab CI/CD will push docker images tagging it with the new git tag pushed to
the repo.

## See also

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## License

- Copyright (c) 2019 Philippe Breucker
- Copyright (c) 2020-2023 Université Gustave Eiffel
- Copyright (c) 2020-2023 INRAE

Licensed under the EUPL.

The full text of the EUPL licence can be found at
https://opensource.org/licenses/EUPL-1.2.
