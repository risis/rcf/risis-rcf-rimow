export const state = () => ({
  datasets: [],
  datasetIndex: {},
  datasetFiles: {},
});

export const mutations = {
  setDatasets(state, data) {
    state.datasets = data;
  },
  setDatasetIndex(state, data) {
    state.datasetIndex = data;
  },
  addDatasets(state, data) {
    data.forEach((dataset) => {
      const index = state.datasetIndex[dataset.id];
      if (index !== undefined) {
        state.datasets[index] = dataset;
      } else {
        const index = state.datasets.length;
        state.datasets.push(dataset);
        state.datasetIndex[dataset.id] = index;
      }
    });
  },
  addDataset(state, data) {
    const index = state.datasetIndex[data.id];
    if (index !== undefined) {
      state.datasets[index] = data;
    } else {
      const index = state.datasets.length;
      state.datasets.push(data);
      state.datasetIndex[data.id] = index;
    }
  },
  setFileFields(state, { datasetId, fileId, fields }) {
    let dsFiles = state.datasetFiles[datasetId];
    if (!dsFiles) {
      dsFiles = {};
    }
    dsFiles[fileId] = fields;
    state.datasetFiles = {
      ...state.datasetFiles,
      [datasetId]: dsFiles,
    };
  },
};

export const actions = {
  async fetchDatasets(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/datasets`,
      // header added by auth library
    });
    const datasets = response.data;
    const index = datasets.reduce((map, current, index) => {
      // Take advantage of loop to assign id property
      current.id = current._id;
      // Actual work of this reduce
      map[current.id] = index;
      return map;
    }, {});
    context.commit('setDatasets', response.data);
    context.commit('setDatasetIndex', index);
  },
  async fetchProjectDatasets(context, { projectId }) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/projects/${projectId}/datasets`,
      // header added by auth library
    });
    // FIXME() Backend should return id instead of _id
    response.data.forEach((ds) => {
      ds.id = ds._id;
    });
    context.commit('addDatasets', response.data);
  },
  async fetchSingleDataset(context, { datasetId }) {
    const encodedId = encodeURIComponent(datasetId);
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/datasets/${encodedId}`,
      // header added by auth library
    });
    // FIXME() Backend should return id instead of _id
    const ds = response.data;
    ds.id = ds._id;
    context.commit('addDataset', ds);
  },
  async fetchFileMetadata(context, { datasetId, fileId }) {
    const encodedDatasetId = encodeURIComponent(datasetId);
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/datasets/${encodedDatasetId}/files/${fileId}/metadata`,
      // header added by auth library
    });
    const meta = response.data;
    if (meta.fields) {
      context.commit('setFileFields', {
        datasetId,
        fileId,
        fields: meta.fields,
      });
    }
  },
  async createDataset(context, { title, files }) {
    try {
      const formData = new FormData();
      formData.append('title', title);
      if (files) {
        files.forEach((file) => {
          formData.append(`files`, file);
        });
      }
      const response = await this.$axios({
        method: 'post',
        url: `${this.$config.apiRoot}/datasets`,
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        // header added by auth library
      });
      context.commit('addDataset', response.data);
      return response.data._id;
    } catch (e) {
      throw new Error(e);
    }
  },
};

export const getters = {
  allDatasets: (state) => state.datasets,
  filterByIds: (state) => (ids) => {
    return state.datasets.filter((ds) => ids.includes(ds.id));
  },
  datasetFiles: (state) => (datasetId) => {
    const ds = state.datasets[state.datasetIndex[datasetId]];
    if (ds && ds.versions) {
      const mainVersionNumber = Object.keys(ds.versions)[0];
      const mainVersion = ds.versions[mainVersionNumber];
      return Object.entries(mainVersion.files).map(([key, value]) => {
        return {
          id: value._id,
          title: key,
          description: value.name,
          tags: [value.type],
        };
      });
    }
    return [];
  },
  datasetFileFields: (state) => (datasetId, fileId) => {
    const dsFiles = state.datasetFiles[datasetId];
    if (dsFiles && dsFiles[fileId]) {
      return dsFiles[fileId];
    }
    return [];
  },
};
