import { set } from 'vue';

export const state = () => ({
  scenarios: [],
  scenarioIndex: {},
  hasErrorByScenario: {},
});

export const mutations = {
  setScenarios(state, data) {
    if (state.scenarios.length) {
      state.scenarios.forEach((scenario) => {
        const index = data.findIndex(
          (dataPartial) => dataPartial.id === scenario.id
        );
        data[index] = scenario;
      });
    }
    state.scenarios = data;
  },
  setScenarioIndex(state, data) {
    state.scenarioIndex = data;
  },
  addScenario(state, data) {
    const index = state.scenarioIndex[data.id];
    if (index !== undefined) {
      state.scenarios[index] = data;
    } else {
      const index = state.scenarios.length;
      state.scenarios.push(data);
      state.scenarioIndex[data.id] = index;
    }
  },
  updateConfig(state, { scenarioId, config }) {
    const index = state.scenarioIndex[scenarioId];
    if (index !== undefined) {
      const scenario = state.scenarios[index];
      const newData = {
        ...scenario,
        config,
      };
      set(state.scenarios, index, newData);
    }
  },
  changeState(state, { scenarioId, newState }) {
    const newData = {
      ...state.hasErrorByScenario[scenarioId],
      ...newState,
    };
    set(state.hasErrorByScenario, scenarioId, newData);
  },
};

export const actions = {
  async fetchScenarios(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/scenarios`,
      // token added by auth library
    });
    const scenarios = response.data;
    const index = scenarios.reduce((map, current, index) => {
      map[current.id] = index;
      return map;
    }, {});
    context.commit('setScenarios', response.data);
    context.commit('setScenarioIndex', index);
  },
  addScenario(context, { scenario }) {
    // FIXME() Backend should return id?
    scenario.id = scenario.name;
    context.commit('addScenario', scenario);
  },
  async fetchProjectScenarios(context, { projectId }) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/projects/${projectId}/scenarios`,
      // token added by auth library
    });
    response.data.forEach((scenario) => {
      context.dispatch('addScenario', { scenario });
    });
  },
  async runScenario(context, { projectId, scenarioId }) {
    const payload = context.rootGetters['parameters/paramPayload'];
    await this.$axios({
      method: 'post',
      url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}/run`,
      data: {
        params: payload,
      },
      // token added by auth library
    });
  },
  async reuseConfig(context, { jobId }) {
    const response = await this.$axios({
      method: 'patch',
      url: `${this.$config.apiRoot}/jobs/${jobId}/config`,
      data: {},
    });
    return response.data;
  },
  async stopScenario(context, { projectId, scenarioId }) {
    await this.$axios({
      method: 'post',
      url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}/actions`,
      data: {
        command: 'STOP',
      },
      // token added by auth library
    });
  },
  updateConfig(context, { scenarioId, config }) {
    context.commit('updateConfig', { context, scenarioId, config });
  },
  changeState(context, { scenarioId, newState }) {
    context.commit('changeState', { scenarioId, newState });
  },
  updateValidation(context, { scenarioId, field, hasError }) {
    context.commit('changeState', {
      scenarioId,
      newState: {
        validations: {
          [field]: hasError,
        },
      },
    });
  },
};

function isParamGroup(param) {
  return !!param.members; // Coerce to boolean
}

function isRequired(param) {
  return !!param.required; // Coerce to boolean
}

function isSet(param, paramKey, config) {
  return (
    config[paramKey] !== undefined ||
    (param.default !== undefined && param.default !== '')
  );
}

function aggregateValidationResults(operator, operandResults) {
  switch (operator) {
    case 'allOf':
      return operandResults.filter(Boolean).length === operandResults.length;
    case 'anyOf':
      return operandResults.filter(Boolean).length >= 1;
    case 'oneOf':
      return operandResults.filter(Boolean).length === 1;
    case 'not': {
      const newVal = operandResults.map((res) => !res);
      return newVal.reduce(
        (acc, currentValue) => acc && currentValue,
        newVal[0]
      );
    }
  }
}

function _validateMembers(operator, memberKeyList, parameter, config) {
  const result = [];
  for (const memberKey of memberKeyList) {
    const member = parameter.members[memberKey];
    if (isParamGroup(member)) {
      const [memberOp, memberOpValue] = _getRequiredOperation(member);
      result.push(
        _isGroupValid(memberOp, memberOpValue, member, config[memberKey])
      );
    } else {
      result.push(isSet(member, memberKey, config));
    }
  }
  return aggregateValidationResults(operator, result);
}

function _validateSubOperations(operator, subOperators, parameter, config) {
  const result = [];
  for (const subOperator in subOperators) {
    const subOperandsValue = subOperators[subOperator];
    result.push(
      _isGroupValid(subOperator, subOperandsValue, parameter, config)
    );
  }
  return aggregateValidationResults(operator, result);
}

function _isGroupValid(parameter, config) {
  const [operator, operandsValue] = _getRequiredOperation(parameter);
  if (Array.isArray(operandsValue)) {
    return _validateMembers(operator, operandsValue, parameter, config);
  } else {
    return _validateSubOperations(operator, operandsValue, parameter, config);
  }
}

function _getRequiredOperation(parameter) {
  const required = parameter.required;
  return Object.entries(required)[0];
}

function isGroupValid(parameter, config) {
  return _isGroupValid(parameter, config);
}

function validateParams(parameter, parameterKey, config) {
  if (isRequired(parameter)) {
    if (isParamGroup(parameter)) {
      const groupIsValid = isGroupValid(parameter, config);
      if (groupIsValid) return;
    } else if (isSet(parameter, parameterKey, config)) {
      return; // there is a value for required parameter
    }
    throw new Error('bad config');
  }
}

export const getters = {
  allScenarios: (state) => state.scenarios,
  singleScenario: (state) => (id) => state.scenarios.find((sc) => sc.id === id),
  hasError: (state) => (scenarioId) => {
    const validations = state.hasErrorByScenario[scenarioId]?.validations;
    if (validations) {
      const hasValidationErrors =
        Object.values(validations).filter(Boolean).length >= 1;
      if (hasValidationErrors) {
        return true;
      }
    }
    const { inputs, providers, config } =
      state.scenarios[state.scenarioIndex[scenarioId]];
    if (inputs) {
      try {
        Object.entries(inputs).forEach(([inputKey, currentInput]) => {
          return validateParams(currentInput, inputKey, config);
        });
      } catch (e) {
        return true;
      }
    }
    if (providers) {
      const providersInputs = Object.entries(providers)
        .filter(([providerKey]) => providerKey !== 'datastore')
        .map(([, providerValue]) => {
          const { params, ...rest } = providerValue;
          return rest;
        });
      try {
        providersInputs.forEach((inputs) => {
          Object.entries(inputs).forEach(([inputKey, currentInput]) => {
            return validateParams(currentInput, inputKey, config);
          });
        });
      } catch (e) {
        return true;
      }
    }
    return false;
  },
};
