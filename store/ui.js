import introJs from 'intro.js';
export const state = () => ({
  date: new Date(),
  links: {
    risis: {
      urlBase: 'https://www.risis2.eu',
      paths: {
        description: '/project-description',
        objectives: '/risis-objectives',
        documents: '/official-documents',
        partners: '/partners',
        board: '/governing-board',
        committees: '/committees',
        datasets: '/risis-datasets',
        registers: '/risis-organisation-registers',
        indicators: '/risis-indicators',
        services: '/risis-services',
        methods: '/risis-methods',
      },
    },
  },
  providerLogos: {
    default: { type: 'icon', src: 'apps' },
    dataverse: { type: 'icon', src: 'apps' },
    cortext: { type: 'image', src: '001s.png' },
    gate: { type: 'image', src: '002s.png' },
  },
  modals: {
    projectImport: {
      object: {},
      type: '',
    },
    datasetCreate: {
      visible: false,
    },
    searchWorkspace: {
      visible: false,
    },
    createPreset: {
      visible: false,
    },
  },
  outputReport: '',
  activeJob: {},
});

export const mutations = {
  setImportObject(state, data) {
    state.modals.projectImport = {
      type: data.type,
      object: data.object,
    };
  },
  setDatasetCreateVisible(state, data) {
    state.modals.datasetCreate = {
      visible: data,
    };
  },
  setSearchWorkspaceVisible(state, data) {
    state.modals.searchWorkspace = {
      visible: data,
    };
  },
  setCreatePresetVisible(state, data) {
    state.modals.createPreset = {
      visible: data,
    };
  },
  setActiveJob(state, data) {
    state.activeJob = data;
  },
  setOutputReport(state, uri) {
    state.outputReport = uri;
  },
  showIntroJs() {
    introJs()
      .setOptions({
        showBullets: false,
      })
      .start();
  },
};

export const actions = {
  async setActiveJob(context, { newActiveJob }) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/jobs/${newActiveJob.id}/config`,
    });
    newActiveJob.config = response.data;
    // FIXME() Check if new active Job exists
    context.commit('setActiveJob', newActiveJob);
  },
};

export const getters = {
  activeJob: (state) => state.activeJob,
  currentYear: (state) => {
    return state.date.getFullYear();
  },
  projectImportModal: (state) => state.modals.projectImport,
  datasetCreateModal: (state) => state.modals.datasetCreate,
  searchWorkspaceModal: (state) => state.modals.searchWorkspace,
  createPresetModal: (state) => state.modals.createPreset,
  links: (state) => (page, key) => {
    const data = state.links[page];
    if (data) {
      const path = data.paths[key];
      if (path) {
        return `${data.urlBase}${path}`;
      }
    }
    return '#';
  },
  outputReport: (state) => state.outputReport,
  mailTo: () => 'rcf-team@risis.io',
};
