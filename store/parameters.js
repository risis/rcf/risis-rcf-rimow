export const state = () => ({
  parameters: [],
  inputMap: {},
  activeSet: {},
  configsOnQueue: {},
});

export const mutations = {
  setInitialParameters(state, data) {
    state.parameters = data;
    if (data.length) {
      let setKey = data[0].key;
      let param = data[0].parameters[0];
      while (param.type === 'group') {
        setKey = param.key;
        param = param.parameters[0];
      }
      state.activeSet = {
        setKey,
      };
    }
  },
  setInputMap(state, data) {
    state.inputMap = data;
  },
  setActiveSet(state, data) {
    state.activeSet = {
      setKey: data.set,
      paramKey: data.param,
    };
  },
  updateParam(state, data) {
    state.inputMap[data.key] = {
      ...state.inputMap[data.key],
      value: data.newValue,
    };
  },
  resetParam(state, data) {
    state.inputMap[data.key] = {
      ...state.inputMap[data.key],
      value: state.inputMap[data.key].default,
    };
  },
  clearParam(state, data) {
    state.inputMap[data.key] = {
      ...state.inputMap[data.key],
      value: undefined,
    };
  },
  removeConfigFromQueue(state, data) {
    const array = state.configsOnQueue[`${data.projectId}/${data.scenarioId}`];
    const index = array.map((e) => e.status).indexOf('updating');
    if (index > -1) {
      array.splice(index, 1);
    }
  },
  setConfigError(state, data) {
    const array = state.configsOnQueue[`${data.projectId}/${data.scenarioId}`];
    const index = array.map((e) => e.status).indexOf('updating');
    if (index > -1) {
      state.configsOnQueue[`${data.projectId}/${data.scenarioId}`][
        index
      ].status = 'error';
    }
  },
  removeErrorUpdatingConfig(state, data) {
    const array = state.configsOnQueue[`${data.projectId}/${data.scenarioId}`];
    const index = array.map((e) => e.status).indexOf('error');
    if (index > -1) {
      array.splice(index, 1);
    }
  },
};

export const actions = {
  async loadParameters(context, { projectId, scenarioId }) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}/config`,
      // header added by auth library
    });
    const presetParams = response.data;
    const scenario =
      context.rootGetters['scenarios/singleScenario'](scenarioId);
    const { providers, inputs, outputs } = scenario;
    const data = [];
    // Parsing
    if (inputs) {
      const taskSets = (inputsGroup) => {
        return Object.entries(inputsGroup)
          .reduce((taskArray, [inputKey, currentInput]) => {
            if (currentInput.members) {
              const inputTasks = {
                key: inputKey,
                title: currentInput.title || inputKey,
                type: 'group',
                providers: [inputKey],
                parameters: taskSets(currentInput.members),
              };
              taskArray.push(inputTasks);
            } else {
              taskArray.push({
                key: inputKey,
                ...currentInput,
              });
            }
            return taskArray;
          }, [])
          .filter((el) => {
            return el != null;
          });
      };
      data.push({
        key: 'inputs',
        title: 'Inputs',
        type: 'group',
        icon: 'step-into',
        providers: ['dataverse'],
        parameters: taskSets(inputs),
      });
    }

    // FIXME() which other things will come here?
    // how to know whether it's an input or a task?
    // for now, key is not 'datastore' and object has attribute other than 'params'
    if (providers) {
      const taskSets = Object.entries(providers)
        .filter(([key, value]) => {
          if (key === 'datastore') {
            return false;
          } else {
            const { params, ...rest } = value;
            return Object.keys(rest).length > 0;
          }
        })
        .reduce((taskArray, [providerKey, currentProvider]) => {
          const { params, ...rest } = currentProvider;
          const providerTasks = Object.entries(rest).map(
            ([taskKey, taskValue]) => {
              return {
                key: taskKey,
                title: taskKey,
                type: 'group',
                providers: [providerKey],
                parameters: Object.entries(taskValue.members).map(
                  ([paramKey, paramValue]) => {
                    return {
                      key: paramKey,
                      ...paramValue,
                    };
                  }
                ),
              };
            }
          );
          taskArray = taskArray.concat(providerTasks);
          return taskArray;
        }, []);
      data.push({
        key: 'tasks',
        title: 'Tasks',
        type: 'group',
        icon: 'subtask',
        parameters: taskSets,
      });
    }

    if (outputs) {
      data.push({
        key: 'outputs',
        title: 'Outputs',
        type: 'group',
        icon: 'browser',
        providers: [],
        parameters: Object.entries(outputs).map(([key, value]) => {
          return {
            ...value,
            key,
          };
        }),
      });
    }

    const inputMap = {};
    // deep copy data object
    const stack = JSON.parse(JSON.stringify(data));
    // Preprocessing before storing
    while (stack.length > 0) {
      const current = stack.pop();
      for (const child of current.parameters) {
        if (child.type === 'group') {
          stack.push(child);
        } else {
          let preset = presetParams[child.key];
          if (child.type === 'string+select') {
            // preprocess string single select
            child.options = child.values;
            child.default = child.values.indexOf(child.default);
            if (preset !== undefined) {
              preset = child.values.indexOf(preset);
              child.value = preset;
            } else {
              child.value = child.default;
            }
          } else if (child.type === 'string+dataset-file-id') {
            // preprocess dataset file id.
            // FIXME() link file-id selector with dataset id selector
            child.datasetSelectKey = 'dataset-id';
            if (child['file-types'] && Array.isArray(child['file-types'])) {
              child.supportedExtensions = child['file-types'];
            }
            if (preset) {
              child.value = preset;
            }
          } else if (child.type === 'string+dataset-file-field') {
            child.fileSelectKey = child.source;
            inputMap[child.source].hasChildren = true;
            if (preset) {
              child.value = preset;
            }
          } else {
            // preprocess string
            // eslint-disable-next-line no-lonely-if
            if (preset !== undefined) {
              child.value = preset;
            } else {
              child.value = child.default;
            }
          }
          // all types are indexed
          inputMap[child.key] = child;
        }
      }
    }
    context.commit('setInitialParameters', data);
    context.commit('setInputMap', inputMap);
  },
  setActiveSet(context, { newActiveSet }) {
    // FIXME() Check if new active set exists
    context.commit('setActiveSet', newActiveSet);
  },
  setErrorUpdatingConfig(context, { projectId, scenarioId }) {
    context.commit('setConfigError', { projectId, scenarioId });
  },
  removeErrorUpdatingConfig(context, { projectId, scenarioId }) {
    context.commit('removeErrorUpdatingConfig', { projectId, scenarioId });
  },
  async update(context, data) {
    // /projects/:projectId/scenarios/:scenarioId/config
    const param = context.state.inputMap[data.key];
    let value = data.newValue;
    // prepare according to type of attribute
    if (param.type === 'string+select') {
      value = param.options[value];
    }
    const requestData = {
      [data.key]: value === undefined || value === null ? '' : value,
    };
    if (param.type === 'range' && value.min === '' && value.max === '') {
      context.commit('updateParam', data);
      return;
    }
    const obj =
      context.state.configsOnQueue[`${data.projectId}/${data.scenarioId}`];
    if (obj) {
      obj.push({ status: 'updating' });
    } else {
      context.state.configsOnQueue[`${data.projectId}/${data.scenarioId}`] = [
        { status: 'updating' },
      ];
    }
    try {
      context.commit('updateParam', data);
      const response = await this.$axios({
        method: 'patch',
        url: `${this.$config.apiRoot}/projects/${data.projectId}/scenarios/${data.scenarioId}/config`,
        data: requestData,
        // header added by auth library
      });
      // FIXME() check existence
      context.dispatch(
        'scenarios/updateConfig',
        {
          config: response.data,
          scenarioId: data.scenarioId,
        },
        { root: true }
      );
      context.commit('removeConfigFromQueue', data);
      return true;
    } catch (e) {
      throw new Error(e);
    }
  },
  switchEnum(context, data) {
    const { key } = data;
    const param = context.state.inputMap[key];
    const currentIndex = param.value;
    const newIndex = (currentIndex + 1) % param.options.length;
    context.dispatch('update', {
      ...data,
      newValue: newIndex,
    });
  },
  updateDatasetSelect(context, data) {
    const { key, newSelected } = data;
    const dependentFields = Object.entries(context.state.inputMap).filter(
      (entry) => {
        return entry[1].value && entry[1].datasetSelectKey === key;
      }
    );
    context.dispatch('update', {
      ...data,
      newValue: newSelected.id,
    });
    dependentFields.forEach((entry) => {
      context.dispatch('update', {
        ...data,
        key: entry[0],
        newValue: undefined,
      });
    });
  },
  updateDatasetFileSelect(context, data) {
    const { newSelected } = data;
    context.dispatch('update', {
      ...data,
      newValue: newSelected.id,
    });
  },
  updateDatasetFileFieldSelect(context, data) {
    const { newSelected } = data;
    context.dispatch('update', {
      ...data,
      newValue: newSelected,
    });
  },
  reset(context, data) {
    const { key } = data;
    const defaultValue = context.state.inputMap[key].default;
    context.dispatch('update', {
      ...data,
      newValue: defaultValue,
    });
  },
};

export const getters = {
  parameters: (state) => state.parameters,
  inputMap: (state) => state.inputMap,
  activeSet: (state) => state.activeSet,
  paramPayload: (state) => {
    function giveValue(parameter) {
      if (parameter.type === 'group') {
        const value = parameter.parameters.reduce((cumulative, current) => {
          cumulative[current.key] = giveValue(current);
          return cumulative;
        }, {});
        return value;
      } else {
        let value = state.inputMap[parameter.key].value;
        if (parameter.type === 'string+select') {
          value = parameter.values[value];
        }
        return value;
      }
    }
    const payload = state.parameters.reduce((cumulative, current) => {
      cumulative[current.key] = giveValue(current);
      return cumulative;
    }, {});
    return payload;
  },
  configsOnQueue: (state) => (projectId, scenarioId) => {
    return state.configsOnQueue[`${projectId}/${scenarioId}`];
  },
};
