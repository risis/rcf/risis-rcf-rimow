export const state = () => ({
  flags: {
    ADVANCED_MODE: false,
  },
});

export const mutations = {
  toggleAdvancedMode(state) {
    state.flags.ADVANCED_MODE = !state.flags.ADVANCED_MODE;
  },
};
