import { set } from 'vue';

export const state = () => ({
  projects: [],
  projectsIndex: {},
  jobTitle: '',
});

export const mutations = {
  setProjects(state, data) {
    state.projects = data;
  },
  setProjectsIndex(state, data) {
    state.projectsIndex = data;
  },
  setJobTitle(state, data) {
    state.jobTitle = data;
  },
  addProject(state, data) {
    const index = state.projectsIndex[data.id];
    if (index !== undefined) {
      set(state.projects, index, data);
    } else {
      const index = state.projects.length;
      state.projects.push(data);
      state.projectsIndex[data.id] = index;
    }
  },
  setProjectProperty(state, data) {
    const index = state.projectsIndex[data.projectId];
    const project = state.projects[index];
    const changed = project.changed ? [...project.changed] : [];
    if (!changed.includes(data.propertyName)) {
      changed.push(data.propertyName);
    }
    set(state.projects, index, {
      ...project,
      changed,
      [data.propertyName]: data.value,
    });
  },
  unSetProjectProperty(state, data) {
    const index = state.projectsIndex[data.projectId];
    const project = state.projects[index];
    let dataChanged = [];
    if (project.changed) {
      const position = project.changed.find((p) => p === data.propertyName);
      if (position !== -1) {
        project.changed.splice(position, 1);
      }
      dataChanged = project.changed;
    }
    set(state.projects, index, {
      ...project,
      dataChanged,
    });
  },
  unlinkScenario(state, data) {
    const index = state.projectsIndex[data.projectId];
    const project = state.projects[index];
    const filteredScenarios = project.scenarios.filter(
      (sc) => sc.id !== data.scenarioId
    );
    let newProject;
    if (project.main_scenario && project.main_scenario.id === data.scenarioId) {
      newProject = (() => {
        const { main_scenario: _, ...filteredProperties } = project;
        return filteredProperties;
      })();
    } else {
      newProject = { ...project };
    }
    set(state.projects, index, {
      ...newProject,
      scenarios: filteredScenarios,
    });
  },
  linkScenario(state, data) {
    const index = state.projectsIndex[data.projectId];
    const project = state.projects[index];
    const scenarios = [...project.scenarios, { id: data.scenarioId }];
    set(state.projects, index, {
      ...project,
      scenarios,
    });
  },
  unlinkDataset(state, data) {
    const index = state.projectsIndex[data.projectId];
    const project = state.projects[index];
    const filteredDatasets = project.datasets.filter(
      (ds) => ds.id !== data.datasetId
    );
    set(state.projects, index, {
      ...project,
      datasets: filteredDatasets,
    });
  },
  linkDataset(state, data) {
    const index = state.projectsIndex[data.projectId];
    const project = state.projects[index];
    const datasets = [...project.datasets, { id: data.datasetId }];
    set(state.projects, index, {
      ...project,
      datasets,
    });
  },
  emptyProjectChanges(state, data) {
    const index = state.projectsIndex[data.id];
    const project = state.projects[index];
    const { changes, ...newProject } = project;
    set(state.projects, index, {
      ...newProject,
    });
  },
};

export const actions = {
  async fetchProjects(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/projects`,
      // header added by auth library
    });
    const projects = response.data;
    const index = projects.reduce((map, current, index) => {
      map[current.id] = index;
      return map;
    }, {});
    context.commit('setProjects', response.data);
    context.commit('setProjectsIndex', index);
  },
  async fetchProjectSummary(context, { id }) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/projects/${id}`,
      params: {
        view: 'summary',
      },
    });
    context.commit('addProject', response.data);
    return response.data;
  },
  async fetchSingleProject(context, { id }) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/projects/${id}`,
      // header added by auth library
    });
    context.commit('addProject', response.data);
    return response.data;
  },
  async unlinkScenario(context, { projectId, scenarioId }) {
    try {
      await this.$axios({
        method: 'delete',
        url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}`,
        // header added by auth library
      });
      context.commit('unlinkScenario', { projectId, scenarioId });
      return true;
    } catch (e) {
      throw new Error(e);
    }
  },
  async changeJobTitle(context, { projectId, scenarioId, title }) {
    try {
      await this.$axios({
        method: 'patch',
        url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}/job`,
        data: {
          title,
        },
        // header added by auth library
      });
      context.commit('setJobTitle', title);
    } catch (e) {
      throw new Error(e);
    }
  },
  setTitleLastJobScenario(context, { projectId, scenarioId }) {
    const project =
      context.state.projects[context.state.projectsIndex[projectId]];
    let jobs;
    if (project.jobs && project.jobs.length > 0) {
      jobs = project.jobs.filter((job) => job.scenario.id === scenarioId);
    } else {
      jobs = project.scenarios.filter(
        (scenario) => scenario.id === scenarioId
      )[0].jobs;
    }
    if (!jobs || jobs.length === 0) {
      context.commit('setJobTitle', '');
      return;
    }
    const copyJobs = [...jobs];
    const lastJob = copyJobs.sort((d1, d2) => {
      return new Date(d2.creation_datetime) - new Date(d1.creation_datetime);
    })[0];
    context.commit('setJobTitle', lastJob.title);
  },
  async linkScenario(context, { projectId, scenarioId }) {
    try {
      const response = await this.$axios({
        method: 'put',
        url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}`,
        // header added by auth library
      });
      const scenarioInstance = await this.$axios({
        method: 'get',
        url: `${this.$config.apiRoot}/projects/${projectId}/scenarios/${scenarioId}`,
        // header added by auth library
      });
      context.dispatch(
        'scenarios/addScenario',
        { scenario: scenarioInstance.data },
        { root: true }
      );
      context.commit('linkScenario', { projectId, scenarioId });
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },
  async unlinkDataset(context, { projectId, datasetId }) {
    try {
      const encodedDatasetId = encodeURIComponent(datasetId);
      await this.$axios({
        method: 'delete',
        url: `${this.$config.apiRoot}/projects/${projectId}/datasets/${encodedDatasetId}`,
        // header added by auth library
      });
      context.commit('unlinkDataset', { projectId, datasetId });
      return true;
    } catch (e) {
      throw new Error(e);
    }
  },
  async linkDataset(context, { projectId, datasetId }) {
    try {
      const encodedDatasetId = encodeURIComponent(datasetId);
      const response = await this.$axios({
        method: 'put',
        url: `${this.$config.apiRoot}/projects/${projectId}/datasets/${encodedDatasetId}`,
        // header added by auth library
      });
      context.commit('linkDataset', { projectId, datasetId });
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },
  async createProject(context, { title, description }) {
    try {
      const response = await this.$axios({
        method: 'post',
        url: `${this.$config.apiRoot}/projects`,
        data: {
          title,
          description,
        },
        // header added by auth library
      });
      context.commit('addProject', response.data);
      return response.data.id;
    } catch (e) {
      throw new Error(e);
    }
  },
  setProjectProperty(context, { projectId, propertyName, value }) {
    context.commit('setProjectProperty', {
      projectId,
      propertyName,
      value,
    });
  },
  unSetProjectProperty(context, { projectId, propertyName }) {
    context.commit('unSetProjectProperty', {
      projectId,
      propertyName,
    });
  },
  async saveProject(context, { id }) {
    const project = context.state.projects[context.state.projectsIndex[id]];
    if (project.changed) {
      const requestData = project.changed.reduce((data, key) => {
        data[key] = project[key].id || project[key];
        return data;
      }, {});
      try {
        await this.$axios({
          method: 'patch',
          url: `${this.$config.apiRoot}/projects/${id}`,
          data: requestData,
          // header added by auth library
        });
        // Project is updated, reset the changes
        context.commit('emptyProjectChanges', { id });
        return true;
      } catch (e) {
        throw new Error(e);
      }
    }
  },
  async deleteProject(context, { projectId }) {
    try {
      await this.$axios({
        method: 'delete',
        url: `${this.$config.apiRoot}/projects/${projectId}`,
        // header added by auth library
      });
      return true;
    } catch (e) {
      throw new Error(e);
    }
  },
};

function projectNotFull(project) {
  return !project || !project.jobs || !project.datasets || !project.scenarios;
}

export const getters = {
  allProjects: (state) => state.projects,
  singleProject: (state) => (id) => {
    return state.projects[state.projectsIndex[id]];
  },
  fullSingleProject: (state, _getters, rootState, rootGetters) => (id) => {
    const project = state.projects[state.projectsIndex[id]];
    if (projectNotFull(project)) return;
    const projectDatasetIds = project.datasets.map((ds) => ds.id);
    const datasets = rootGetters['datasets/filterByIds'](projectDatasetIds);
    const projectScenarioIds = project.scenarios.map((sc) => sc.id);
    const scenarios = rootState.scenarios.scenarios.filter((ds) =>
      projectScenarioIds.includes(ds.id)
    );
    return {
      project,
      datasets,
      scenarios,
    };
  },
  projectDatasets: (state, _getters, _rootState, rootGetters) => (id) => {
    const project = state.projects[state.projectsIndex[id]];
    const projectDatasetIds = project.datasets.map((ds) => ds.id);
    return rootGetters['datasets/filterByIds'](projectDatasetIds);
  },
  projectRunningScenario: (state, _getters) => (projectId) => {
    const project = _getters.singleProject(projectId);
    const jobs = project.jobs;
    if (jobs && jobs.length) {
      const runningJob = jobs.find((job) => job.state.slug === 'running');
      if (runningJob) {
        const scenario = project.scenarios.find(
          (scenario) => scenario.id === runningJob.scenario.id
        );
        if (scenario) {
          return scenario;
        }
      }
    }
    return {};
  },
  getTitleLastJobScenario: (state) => {
    return state.jobTitle;
  },
};
