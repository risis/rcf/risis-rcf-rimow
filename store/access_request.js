export const actions = {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async fetchDatasets(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/open_access/datasets`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async fetchDataset(context, data) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/open_access/datasets/${data.datasetId}/metadata`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async fetchAccessRequestsEvaluationList(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/access_requests/evaluations`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async fetchAccessRequests(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/access_requests/all`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async fetchMyAccessRequests(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/access_requests/my`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async fetchAccessRequest(context, id) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/access_requests/${id}/data`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async evaluationDetails(context, id) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/access_requests/${id}/evaluation`,
      // header added by auth library
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async createAccessRequest(context, data) {
    try {
      const response = await this.$axios({
        method: 'post',
        url: `${this.$config.apiRoot}/access_requests/new`,
        data,
      });
      return response.data;
    } catch (e) {
      throw new Error(e);
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async submitCv(context, data) {
    try {
      const formData = new FormData();
      formData.append('title', 'test cv');
      formData.append(`files`, data.file);
      const response = await this.$axios({
        method: 'post',
        url: `${this.$config.apiRoot}/access_requests/${data.id}/cv`,
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async submitEvaluation(context, data) {
    const response = await this.$axios({
      method: 'post',
      url: `${this.$config.apiRoot}/access_requests/${data.id}/evaluation/submit`,
      data,
      // header added by auth library
    });
    const projects = response.data;
    return projects;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async downloadCv(context, accessRequest) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/access_requests/${accessRequest.id}/cv`,
      // header added by auth library
    });
    return response;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async submitRevision(context, accessRequest) {
    const response = await this.$axios({
      method: 'post',
      url: `${this.$config.apiRoot}/access_requests/${accessRequest.id}/revise`,
      data: accessRequest,
    });
    return response.data;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async cancelAccessRequest(context, accessRequest) {
    const response = await this.$axios({
      method: 'post',
      url: `${this.$config.apiRoot}/access_requests/${accessRequest.id}/cancel`,
      data: accessRequest,
    });
    return response.data;
  },
};
