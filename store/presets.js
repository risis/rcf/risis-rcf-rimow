export const state = () => ({
  presets: [],
  userPresetLoaded: null,
});

export const mutations = {
  setPresets(state, data) {
    state.presets = data;
  },
  setUserPreset(state, data) {
    state.userPresetLoaded = data;
  },
  updateUserPreset(state, data) {
    const { label, description, query, presetId } = data;
    state.userPresetLoaded = {
      ...state.userPresetLoaded,
      ...(label && { label }),
      ...(description && { description }),
      ...(query && { query }),
      ...(presetId && { presetId }),
    };
  },
};

export const actions = {
  async getPresets(context, { scenarioId }) {
    try {
      const response = await this.$axios({
        method: 'get',
        url: `${this.$config.apiRoot}/scenarios/${scenarioId}/presets`,
        // header added by auth library
      });
      context.commit('setPresets', response.data);
    } catch (e) {
      throw new Error(e);
    }
  },
  async createPresets(context, { scenarioId, title, description, query }) {
    try {
      return await this.$axios({
        method: 'post',
        url: `${this.$config.apiRoot}/scenarios/${scenarioId}/presets`,
        data: { title, description, query },
        // header added by auth library
      });
    } catch (e) {
      throw new Error(e);
    }
  },
  async deletePreset(context, { scenarioId, presetId }) {
    try {
      const response = await this.$axios({
        method: 'delete',
        url: `${this.$config.apiRoot}/scenarios/${scenarioId}/presets/${presetId}`,
        // header added by auth library
      });
      return response;
    } catch (e) {
      throw new Error(e);
    }
  },
  async updatePresets(
    context,
    { scenarioId, presetId, title, description, query }
  ) {
    try {
      return await this.$axios({
        method: 'patch',
        url: `${this.$config.apiRoot}/scenarios/${scenarioId}/presets/${presetId}`,
        data: { title, description, query },
        // header added by auth library
      });
    } catch (e) {
      throw new Error(e);
    }
  },
};

export const getters = {
  allUserPresets: (state) => state.presets,
  getUserPreset: (state) => state.userPresetLoaded,
};
