export const state = () => ({
  notifications: [],
  notificationsIndex: {},
  seen: true,
});

export const mutations = {
  setNotifications(state, data) {
    if (data.find((n) => !n.seen)) {
      state.seen = false;
    }
    state.notifications = data;
  },
  addNotification(state, data) {
    state.seen = false;
    state.notifications.unshift(data);
  },
  deleteNotifications(state, data) {
    const index = state.notifications.findIndex(
      (n) => n.id === data.notificationId
    );
    state.notifications.splice(index, 1);
  },
  markAsSeen(state) {
    state.seen = true;
    state.notifications.forEach((notification) => {
      notification.seen = true;
    });
  },
};

export const actions = {
  async fetchNotifications(context) {
    const response = await this.$axios({
      method: 'get',
      url: `${this.$config.apiRoot}/notifications?dir=desc&order=creation_datetime`,
      // token added by auth library
    });
    context.commit('setNotifications', response.data);
  },
  async deleteNotification(context, { notificationId }) {
    await this.$axios({
      method: 'delete',
      url: `${this.$config.apiRoot}/notifications/${notificationId}`,
      // token added by auth library
    });
    context.commit('deleteNotifications', { notificationId });
  },
  async addNotification(context, { title, details, url, type, reason }) {
    const response = await this.$axios({
      method: 'post',
      url: `${this.$config.apiRoot}/notifications/`,
      data: { title, details, url, type, reason },
      // token added by auth library
    });
    context.commit('addNotification', response.data);
  },
  addNotificationMenu(context, notification) {
    context.commit('addNotification', notification);
  },
  async markAsSeen(context) {
    await this.$axios({
      method: 'patch',
      url: `${this.$config.apiRoot}/notifications/seen`,
      data: {},
      // token added by auth library
    });
    context.commit('markAsSeen');
  },
};

export const getters = {
  all: (state) => state.notifications,
  seen: (state) => state.seen,
};
