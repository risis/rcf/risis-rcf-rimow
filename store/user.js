export const state = () => ({
  stats: {},
  profile: {},
});

export const mutations = {
  profileUser(state, data) {
    state.profile = data;
  },
  userStats(state, data) {
    data.miniCards = [];
    const dataError = function (obj) {
      return {
        title: `Error getting ${obj} data`,
        bg: 'bg-red',
        icon: 'AlertTriangleIcon',
      };
    };
    data.miniCards = [
      data.projects.error
        ? dataError('projects')
        : {
            title: `${data.projects.count || '0'} projects`,
            subtitle: `${data.projects.owned || '0'} owned`,
            bg: 'bg-projects',
            icon: 'PackageIcon',
          },
      data.scenarios.error
        ? dataError('scenarios')
        : {
            title: `${data.scenarios.count || '0'} scenarios`,
            subtitle: `${data.scenarios.imported || '0'} in projects`,
            bg: 'bg-scenarios',
            icon: 'RouteIcon',
          },
      data.datasets.error
        ? dataError('datasets')
        : {
            title: `${data.datasets.count || '0'} datasets`,
            subtitle: `${data.datasets.imported || '0'} in projects`,
            bg: 'bg-datasets',
            icon: 'DatabaseIcon',
          },
      data.jobs.error
        ? dataError('jobs')
        : {
            title: `${data.jobs.executing || '0'} tasks executing`,
            subtitle: `${data.jobs.waiting || '0'} waiting`,
            bg: 'bg-console',
            icon: 'ActivityIcon',
            completedSteps: Math.floor(
              (data.jobs.executing /
                (data.jobs.executing + data.jobs.waiting)) *
                100
            ),
          },
    ];
    state.stats = data;
  },
};

export const actions = {
  async getUserStats(context) {
    try {
      const response = await this.$axios({
        method: 'get',
        url: `${this.$config.apiRoot}/users/stats`,
        // header added by auth library
      });
      context.commit('userStats', response.data);
      return true;
    } catch (e) {
      throw new Error(e);
    }
  },
  async getProfileUser(context) {
    try {
      const response = await this.$axios({
        method: 'get',
        url: `${this.$config.apiRoot}/users/profile`,
        // header added by auth library
      });
      context.commit('profileUser', response.data);
      return true;
    } catch (e) {
      throw new Error(e);
    }
  },
};

export const getters = {
  profile: (state) => state.profile,
  stats: (state) => state.stats,
};
