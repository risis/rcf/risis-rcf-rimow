export default function ({ $axios, error: nuxtError, redirect, $auth }) {
  $axios.onError((error) => {
    if ($auth && (!$auth.user || !$auth.loggedIn)) {
      redirect('/login');
    } else {
      nuxtError({
        statusCode: error.response.status,
      });
    }
  });
}
