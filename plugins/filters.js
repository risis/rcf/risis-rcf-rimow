import Vue from 'vue';
import { parseISO, format, formatDistanceToNow } from 'date-fns';

// FIXME() How to handle type errors in filters?

Vue.filter('timeago', (date) => {
  if (!date) return '';
  let parsedDate = date;
  if (!(date instanceof Date)) {
    parsedDate = parseISO(date);
  }
  return formatDistanceToNow(parsedDate, { addSuffix: true });
});

Vue.filter('formatDate', (date, pattern) => {
  if (!date) return '';
  let parsedDate = date;
  if (!(date instanceof Date)) {
    parsedDate = parseISO(date);
  }
  return format(parsedDate, pattern);
});

Vue.filter('stripComma', (text) => {
  if (typeof text === 'string' || text instanceof String) {
    return text.replace(',', '');
  }
  return '';
});

Vue.filter('capitalize', (text) => {
  if (typeof text === 'string' || text instanceof String) {
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
  }
  return '';
});

// Based on @see https://github.com/imcvampire/vue-truncate-filter
Vue.filter('truncateText', (text = '', length = 30, clamp = '...') => {
  if (typeof text === 'string' || text instanceof String) {
    if (text.length < length) {
      return text;
    }
    return text.slice(0, length - clamp.length) + clamp;
  }
  return '';
});

Vue.filter('userInitials', (user) => {
  const text =
    user.firstName || user.first_name || user.user_name || user.username;
  return text[0].toUpperCase();
});

Vue.filter('wordInitialLetter', (word) => {
  if (word && (typeof word === 'string' || word instanceof String)) {
    const letter = word.match(/[a-z0-9]/i);
    if (letter && letter.length > 0) {
      return letter[0].toUpperCase();
    }
  }
  return '';
});
