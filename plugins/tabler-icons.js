import {
  ActivityIcon,
  AdjustmentsHorizontalIcon,
  AdjustmentsIcon,
  AlertCircleIcon,
  AlertTriangleIcon,
  AntennaBars1Icon,
  AppsIcon,
  ArchiveIcon,
  ArrowDownCircleIcon,
  ArrowLeftIcon,
  ArrowRightIcon,
  ArrowUpCircleIcon,
  BellIcon,
  BellXIcon,
  BinaryIcon,
  BrandGitlabIcon,
  BrowserIcon,
  BugIcon,
  BulbIcon,
  CalculatorIcon,
  CalendarIcon,
  ChartBarIcon,
  CheckIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  CircleCheckIcon,
  CircleOffIcon,
  ClearFormattingIcon,
  ClipboardTextIcon,
  ClockIcon,
  ColorSwatchIcon,
  ColumnsIcon,
  CopyIcon,
  DatabaseIcon,
  DeviceFloppyIcon,
  DiamondIcon,
  DiceIcon,
  DirectionsIcon,
  DotsVerticalIcon,
  DownloadIcon,
  ExternalLinkIcon,
  EyeIcon,
  FileExportIcon,
  FileIcon,
  FileLikeIcon,
  FilesOffIcon,
  FileTextIcon,
  FileZipIcon,
  FolderIcon,
  FolderPlusIcon,
  FormsIcon,
  FunctionIcon,
  HeadsetIcon,
  HelpIcon,
  HomeIcon,
  InfoCircleIcon,
  LayoutGridIcon,
  LayoutListIcon,
  LetterCaseIcon,
  ListIcon,
  ListSearchIcon,
  LivePhotoIcon,
  LogicAndIcon,
  LogicNandIcon,
  LogicNorIcon,
  LogicOrIcon,
  LogoutIcon,
  MapIcon,
  MaximizeIcon,
  MessageIcon,
  NumberIcon,
  NumbersIcon,
  PackageIcon,
  PackagesIcon,
  PlayerPlayIcon,
  PlayerStopIcon,
  PlusIcon,
  PresentationIcon,
  QuestionMarkIcon,
  RectangleVerticalIcon,
  RefreshIcon,
  RepeatIcon,
  RouteIcon,
  ScreenShareIcon,
  SearchIcon,
  SelectIcon,
  SettingsIcon,
  ShareIcon,
  SquarePlusIcon,
  StackIcon,
  StarIcon,
  StepIntoIcon,
  SubtaskIcon,
  SwitchHorizontalIcon,
  TableAliasIcon,
  TableShortcutIcon,
  TagIcon,
  TargetIcon,
  TemplateIcon,
  TextPlusIcon,
  ToggleRightIcon,
  TrashIcon,
  UploadIcon,
  UserIcon,
  UserMinusIcon,
  UserPlusIcon,
  UsersIcon,
  VectorTriangleIcon,
  VersionsIcon,
  ViewfinderIcon,
  XIcon,
} from 'vue-tabler-icons';

import Vue from 'vue';

Vue.component('ActivityIcon', ActivityIcon);
Vue.component('AdjustmentsHorizontalIcon', AdjustmentsHorizontalIcon);
Vue.component('AdjustmentsIcon', AdjustmentsIcon);
Vue.component('AlertCircleIcon', AlertCircleIcon);
Vue.component('AlertTriangleIcon', AlertTriangleIcon);
Vue.component('AntennaBars1Icon', AntennaBars1Icon);
Vue.component('AppsIcon', AppsIcon);
Vue.component('ArchiveIcon', ArchiveIcon);
Vue.component('ArrowDownCircleIcon', ArrowDownCircleIcon);
Vue.component('ArrowLeftIcon', ArrowLeftIcon);
Vue.component('ArrowRightIcon', ArrowRightIcon);
Vue.component('ArrowUpCircleIcon', ArrowUpCircleIcon);
Vue.component('BellIcon', BellIcon);
Vue.component('BellXIcon', BellXIcon);
Vue.component('BinaryIcon', BinaryIcon);
Vue.component('BrandGitlabIcon', BrandGitlabIcon);
Vue.component('BrowserIcon', BrowserIcon);
Vue.component('BugIcon', BugIcon);
Vue.component('BulbIcon', BulbIcon);
Vue.component('CalculatorIcon', CalculatorIcon);
Vue.component('CalendarIcon', CalendarIcon);
Vue.component('ChartBarIcon', ChartBarIcon);
Vue.component('CheckIcon', CheckIcon);
Vue.component('ChevronDownIcon', ChevronDownIcon);
Vue.component('ChevronLeftIcon', ChevronLeftIcon);
Vue.component('ChevronRightIcon', ChevronRightIcon);
Vue.component('CircleCheckIcon', CircleCheckIcon);
Vue.component('CircleOffIcon', CircleOffIcon);
Vue.component('ClearFormattingIcon', ClearFormattingIcon);
Vue.component('ClipboardTextIcon', ClipboardTextIcon);
Vue.component('ClockIcon', ClockIcon);
Vue.component('ColorSwatchIcon', ColorSwatchIcon);
Vue.component('ColumnsIcon', ColumnsIcon);
Vue.component('CopyIcon', CopyIcon);
Vue.component('DatabaseIcon', DatabaseIcon);
Vue.component('DeviceFloppyIcon', DeviceFloppyIcon);
Vue.component('DiamondIcon', DiamondIcon);
Vue.component('DiceIcon', DiceIcon);
Vue.component('DirectionsIcon', DirectionsIcon);
Vue.component('DotsVerticalIcon', DotsVerticalIcon);
Vue.component('DownloadIcon', DownloadIcon);
Vue.component('ExternalLinkIcon', ExternalLinkIcon);
Vue.component('EyeIcon', EyeIcon);
Vue.component('FileExportIcon', FileExportIcon);
Vue.component('FileIcon', FileIcon);
Vue.component('FileLikeIcon', FileLikeIcon);
Vue.component('FilesOffIcon', FilesOffIcon);
Vue.component('FileTextIcon', FileTextIcon);
Vue.component('FileZipIcon', FileZipIcon);
Vue.component('FolderIcon', FolderIcon);
Vue.component('FolderPlusIcon', FolderPlusIcon);
Vue.component('FormsIcon', FormsIcon);
Vue.component('FunctionIcon', FunctionIcon);
Vue.component('HeadsetIcon', HeadsetIcon);
Vue.component('HelpIcon', HelpIcon);
Vue.component('HomeIcon', HomeIcon);
Vue.component('InfoCircleIcon', InfoCircleIcon);
Vue.component('LayoutGridIcon', LayoutGridIcon);
Vue.component('LayoutListIcon', LayoutListIcon);
Vue.component('LetterCaseIcon', LetterCaseIcon);
Vue.component('ListIcon', ListIcon);
Vue.component('ListSearchIcon', ListSearchIcon);
Vue.component('LivePhotoIcon', LivePhotoIcon);
Vue.component('LogicAndIcon', LogicAndIcon);
Vue.component('LogicNandIcon', LogicNandIcon);
Vue.component('LogicNorIcon', LogicNorIcon);
Vue.component('LogicOrIcon', LogicOrIcon);
Vue.component('LogoutIcon', LogoutIcon);
Vue.component('MapIcon', MapIcon);
Vue.component('MaximizeIcon', MaximizeIcon);
Vue.component('MessageIcon', MessageIcon);
Vue.component('NumberIcon', NumberIcon);
Vue.component('NumbersIcon', NumbersIcon);
Vue.component('PackageIcon', PackageIcon);
Vue.component('PackagesIcon', PackagesIcon);
Vue.component('PlayerPlayIcon', PlayerPlayIcon);
Vue.component('PlayerStopIcon', PlayerStopIcon);
Vue.component('PlusIcon', PlusIcon);
Vue.component('PresentationIcon', PresentationIcon);
Vue.component('QuestionMarkIcon', QuestionMarkIcon);
Vue.component('RectangleVerticalIcon', RectangleVerticalIcon);
Vue.component('RefreshIcon', RefreshIcon);
Vue.component('RepeatIcon', RepeatIcon);
Vue.component('RouteIcon', RouteIcon);
Vue.component('ScreenShareIcon', ScreenShareIcon);
Vue.component('SearchIcon', SearchIcon);
Vue.component('SelectIcon', SelectIcon);
Vue.component('SettingsIcon', SettingsIcon);
Vue.component('ShareIcon', ShareIcon);
Vue.component('SquarePlusIcon', SquarePlusIcon);
Vue.component('StackIcon', StackIcon);
Vue.component('StarIcon', StarIcon);
Vue.component('StepIntoIcon', StepIntoIcon);
Vue.component('SubtaskIcon', SubtaskIcon);
Vue.component('SwitchHorizontalIcon', SwitchHorizontalIcon);
Vue.component('TableAliasIcon', TableAliasIcon);
Vue.component('TableShortcutIcon', TableShortcutIcon);
Vue.component('TagIcon', TagIcon);
Vue.component('TargetIcon', TargetIcon);
Vue.component('TemplateIcon', TemplateIcon);
Vue.component('TextPlusIcon', TextPlusIcon);
Vue.component('ToggleRightIcon', ToggleRightIcon);
Vue.component('TrashIcon', TrashIcon);
Vue.component('UploadIcon', UploadIcon);
Vue.component('UserIcon', UserIcon);
Vue.component('UserMinusIcon', UserMinusIcon);
Vue.component('UserPlusIcon', UserPlusIcon);
Vue.component('UsersIcon', UsersIcon);
Vue.component('VectorTriangleIcon', VectorTriangleIcon);
Vue.component('VersionsIcon', VersionsIcon);
Vue.component('ViewfinderIcon', ViewfinderIcon);
Vue.component('XIcon', XIcon);
