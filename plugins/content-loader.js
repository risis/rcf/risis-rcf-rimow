import Vue from 'vue';
import { ContentLoader } from 'vue-content-loader';

Vue.component('ContentLoader', {
  extends: ContentLoader,
  props: {
    primaryColor: {
      type: String,
      default: '#e6e5ef',
    },
    secondaryColor: {
      type: String,
      default: '#f2f3f9',
    },
  },
});

// Vue.component('ContentLoader', ContentLoader);
