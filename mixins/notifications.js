export default {
  methods: {
    showNotification(message, title, variant = 'success', to = null) {
      this.$root.$bvToast.toast(message, {
        title,
        variant,
        solid: true,
        to,
      });
    },
    showErrorNotification(
      message,
      title = 'Oops... something went wrong!',
      to = null
    ) {
      this.$root.$bvToast.toast(message, {
        title,
        variant: 'danger',
        solid: true,
        to,
      });
    },
  },
};
