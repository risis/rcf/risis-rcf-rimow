export default {
  methods: {
    searchFunction(valueToSearch, listObjects, filters) {
      let listFiltered = [];
      if (!valueToSearch) {
        listFiltered = listObjects;
      } else {
        listFiltered = listObjects.filter((object) => {
          for (let i = 0; i < filters.filterBy.length; i++) {
            const filterBy = filters.filterBy[i].filterBy;
            const value = this.getDeepValue(object, filterBy.toLowerCase());
            if (
              value &&
              value.toLowerCase().includes(valueToSearch.toLowerCase())
            ) {
              return true;
            }
          }
          return false;
        });
      }
      return listFiltered;
    },
    applyFilterParameter(parameters, searchTerm) {
      const counter = { count: 0 };
      const matches = this._applyFilterParameter(
        parameters,
        searchTerm.toLowerCase(),
        counter
      );
      return { matches, count: counter.count };
    },
    _applyFilterParameter(parameters, searchTerm, counter) {
      const matches = [];
      if (!Array.isArray(parameters)) return matches;
      parameters.forEach((i) => {
        if (
          !i.parameters &&
          (searchTerm === '' || i.title.toLowerCase().includes(searchTerm))
        ) {
          counter.count++;
          matches.push(i);
        } else {
          const childResults = this._applyFilterParameter(
            i.parameters,
            searchTerm,
            counter
          );
          if (childResults.length)
            matches.push(Object.assign({}, i, { parameters: childResults }));
        }
      });
      return matches;
    },
    applyFilter(filters, listObjects) {
      const copyObject = [...listObjects];
      return copyObject.sort((obj1, obj2) => {
        // FIXME() There is no Author and Date to order by in Datasets.
        if (
          this.getDeepValue(obj1, filters.orderBy.toLowerCase()) <
          this.getDeepValue(obj2, filters.orderBy.toLowerCase())
        ) {
          return filters.direction === 'Ascending' ? -1 : 1;
        }
        if (
          this.getDeepValue(obj1, filters.orderBy.toLowerCase()) >
          this.getDeepValue(obj2, filters.orderBy.toLowerCase())
        ) {
          return filters.direction === 'Ascending' ? 1 : -1;
        }
        return 0;
      });
    },
    getDeepValue(obj, path) {
      const pathSplitted = path.split('.');
      for (let i = 0; i < pathSplitted.length; i++) {
        if (pathSplitted[i].includes("'")) {
          let accumulatedPath = pathSplitted[i] + '.';
          do {
            i++;
            accumulatedPath += pathSplitted[i] + '.';
          } while (!pathSplitted[i].includes("'"));
          accumulatedPath = accumulatedPath.substring(
            1,
            accumulatedPath.length - 2
          );
          obj = obj[accumulatedPath];
        } else {
          obj = obj[pathSplitted[i]];
        }
        if (!obj) {
          return '';
        }
      }
      return Array.isArray(obj) ? obj[0] : obj;
    },
  },
};
