// queryFactory.js

const inputTypes = {
  text: {
    onConditions: [
      'equal',
      'not_equal',
      'like',
      'not_like',
      'starts_with',
      'ends_with',
      'is_empty',
      'is_not_empty',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  textarea: {
    onConditions: [
      'equal',
      'not_equal',
      'like',
      'not_like',
      'starts_with',
      'ends_with',
      'is_empty',
      'is_not_empty',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  number: {
    onConditions: [
      'equal',
      'not_equal',
      'less',
      'less_or_equal',
      'greater',
      'greater_or_equal',
      'between',
      'not_between',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  slider: {
    onConditions: [
      'equal',
      'not_equal',
      'less',
      'less_or_equal',
      'greater',
      'greater_or_equal',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  date: {
    onConditions: [
      'equal',
      'not_equal',
      'less',
      'less_or_equal',
      'greater',
      'greater_or_equal',
      'between',
      'not_between',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  time: {
    onConditions: [
      'equal',
      'not_equal',
      'less',
      'less_or_equal',
      'greater',
      'greater_or_equal',
      'between',
      'not_between',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  datetime: {
    onConditions: [
      'equal',
      'not_equal',
      'less',
      'greater',
      'greater_or_equal',
      'between',
      'not_between',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear'],
  },
  select: {
    onConditions: [
      'select_equals',
      'select_not_equals',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear', 'switch'],
  },
  multiselect: {
    onConditions: [
      'select_any_in',
      'select_not_any_in',
      'is_null',
      'is_not_null',
    ],
    withActions: ['reset', 'clear', 'switch'],
  },
  boolean: {
    onConditions: ['equal', 'not_equal', 'is_null', 'is_not_null'],
    withActions: ['reset', 'clear'],
  },
};

const inputActions = ['reset', 'clear', 'switch'];

const defaultInputType = 'text';

const operators = [
  {
    name: 'AND',
    identifier: 'AND',
    icon: 'and',
    description: 'All conditions must be true',
  },
  {
    name: 'NOT (AND)',
    identifier: 'NOT_AND',
    icon: 'nand',
    description: 'All conditions must be false',
  },
  {
    name: 'OR',
    identifier: 'OR',
    icon: 'or',
    description: 'At least one of the conditions must be true',
  },
  {
    name: 'NOT (OR)',
    identifier: 'NOT_OR',
    icon: 'nor',
    description: 'At least one of the conditions must be false',
  },
];

const conditions = [
  {
    id: 'equal',
    name: 'Equal',
    humanOp: '=',
    isNotOp: false,
    cardinality: 1,
    onTypes: [
      'text',
      'textarea',
      'number',
      'slider',
      'date',
      'time',
      'datetime',
      'boolean',
    ],
  },
  {
    id: 'not_equal',
    name: 'Not equal',
    humanOp: '!=',
    isNotOp: true,
    cardinality: 1,
    onTypes: [
      'text',
      'textarea',
      'number',
      'slider',
      'date',
      'time',
      'datetime',
      'boolean',
    ],
  },
  {
    id: 'less',
    name: 'Less',
    humanOp: '<',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['number', 'slider', 'date', 'time', 'datetime'],
  },
  {
    id: 'less_or_equal',
    name: 'Less or equal',
    humanOp: '<=',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['number', 'slider', 'date', 'time', 'datetime'],
  },
  {
    id: 'greater',
    name: 'Greater',
    humanOp: '>',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['number', 'slider', 'date', 'time', 'datetime'],
  },
  {
    id: 'greater_or_equal',
    name: 'Greater or equal',
    humanOp: '>=',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['number', 'slider', 'date', 'time', 'datetime'],
  },
  {
    id: 'like',
    name: 'Contains',
    humanOp: 'CONTAINS',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['text', 'textarea'],
  },
  {
    id: 'not_like',
    name: 'Not contains',
    humanOp: 'NOT CONTAINS',
    isNotOp: true,
    cardinality: 1,
    onTypes: ['text', 'textarea'],
  },
  {
    id: 'starts_with',
    name: 'Starts with',
    humanOp: 'STARTS WITH',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['text', 'textarea'],
  },
  {
    id: 'ends_with',
    name: 'Ends with',
    humanOp: 'ENDS WITH',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['text', 'textarea'],
  },
  {
    id: 'between',
    name: 'Between',
    humanOp: 'BETWEEN',
    isNotOp: false,
    cardinality: 2,
    onTypes: ['number', 'date', 'time', 'datetime'],
  },
  {
    id: 'not_between',
    name: 'Not between',
    humanOp: 'NOT BETWEEN',
    isNotOp: true,
    cardinality: 2,
    onTypes: ['number', 'date', 'time', 'datetime'],
  },
  {
    id: 'is_empty',
    name: 'Is empty',
    humanOp: 'IS EMPTY',
    isNotOp: false,
    cardinality: 0,
    onTypes: ['text', 'textarea'],
  },
  {
    id: 'is_not_empty',
    name: 'Is not empty',
    humanOp: 'IS NOT EMPTY',
    isNotOp: true,
    cardinality: 0,
    onTypes: ['text', 'textarea'],
  },
  {
    id: 'select_equals',
    name: 'Equal',
    humanOp: '==',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['select'],
  },
  {
    id: 'select_not_equals',
    name: 'Not equal',
    humanOp: '!=',
    isNotOp: true,
    cardinality: 1,
    onTypes: ['select'],
  },
  {
    id: 'is_null',
    name: 'Is null',
    humanOp: 'IS NULL',
    isNotOp: false,
    cardinality: 0,
    onTypes: [
      'text',
      'textarea',
      'number',
      'slider',
      'date',
      'time',
      'datetime',
      'select',
      'multiselect',
      'boolean',
    ],
  },
  {
    id: 'is_not_null',
    name: 'Is not null',
    humanOp: 'IS NOT NULL',
    isNotOp: true,
    cardinality: 0,
    onTypes: [
      'text',
      'textarea',
      'number',
      'slider',
      'date',
      'time',
      'datetime',
      'select',
      'multiselect',
      'boolean',
    ],
  },
  {
    id: 'select_any_in',
    name: 'Any in',
    humanOp: 'IN',
    isNotOp: false,
    cardinality: 1,
    onTypes: ['multiselect'],
  },
  {
    id: 'select_not_any_in',
    name: 'Not in',
    humanOp: 'NOT IN',
    isNotOp: true,
    cardinality: 1,
    onTypes: ['multiselect'],
  },
];

const fieldTypeIconsMap = {
  text: 'forms',
  textarea: 'text-plus',
  number: 'numbers',
  slider: 'adjustments-horizontal',
  date: 'calendar',
  time: 'clock',
  datetime: 'calendar',
  select: 'select',
  multiselect: 'color-swatch',
  boolean: 'toggle-right',
  default: 'forms',
};

const identifierPartLevelName = {
  0: 'sources',
  1: 'entities',
  2: 'fields',
};

const identifierTypeIconsMap = {
  sources: 'packages',
  entities: 'layout-grid',
  fields: 'arrow-right',
  default: 'list-icon',
};

const identifierKind = (id) => {
  return identifierPartLevelName[id.match(/\./g).length] || 'default';
};

const fieldTypeIconName = (type) => {
  return type
    ? fieldTypeIconsMap[type] || identifierTypeIconsMap.fields
    : identifierTypeIconsMap.default;
};

const identifierPartIconName = (part) => {
  return part ? identifierTypeIconsMap[part] : identifierTypeIconsMap.default;
};

const identifierPartLabelColor = {
  sources: 'indigo',
  entities: 'black',
  fields: 'purple',
  default: 'secondary',
};

export default {
  operators,
  conditions,
  inputTypes,
  inputActions,
  defaultInputType,
  identifierKind,
  identifierPartLevelName,
  identifierPartIconName,
  identifierPartLabelColor,
  fieldTypeIconName,
  fieldTypeIconsMap,
};
